using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class HealthBarTest
{
    private HealthBar _healthBar;
    private const float StartSize = 10f;
    
    [OneTimeSetUp]
    public void Init()
    {
        _healthBar = new GameObject().AddComponent<HealthBar>();
        _healthBar.transform.localScale = Vector3.one * StartSize;
    }

    [Test]
    public void HealthBarResetTest()
    {
        
        _healthBar.Reset();
        Assert.AreEqual(StartSize, _healthBar.transform.localScale.x);
    }
    
}
