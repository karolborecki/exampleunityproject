using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class DataManagerTest
{
    private const string DataSaveTag = "scores";
    
    [Test]
    public void DataManagerReadTest()
    {
        const string dataToSave = "0 1 2 3";
        PlayerPrefs.SetString(DataSaveTag, dataToSave);
        var dataRead = DataManager.GetScoreData();
        Assert.AreEqual(dataToSave, dataRead);
    }
    
    [Test]
    public void DataManagerSaveTest()
    {
        PlayerPrefs.DeleteAll();
        DataManager.AddScoreData(1);
        var dataAfterTest = DataManager.GetScoreData();
        Assert.AreEqual("1 ", dataAfterTest);
    }
    
    [Test]
    public void DataManagerSaveTest2()
    {
        PlayerPrefs.DeleteAll();
        DataManager.AddScoreData(-1);
        var dataAfterTest = DataManager.GetScoreData();
        Assert.AreEqual("", dataAfterTest);
    }
}
