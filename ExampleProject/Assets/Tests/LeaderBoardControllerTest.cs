using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;


public class LeaderBoardControllerTest
{
    private LeaderboardController _leaderboard;
    
    [OneTimeSetUp]
    public void Init()
    {
        _leaderboard = new GameObject().AddComponent<LeaderboardController>();
    }
    
    [Test]
    public void LeaderBoardControllerGettingFormattedListTest()
    {
        var data = _leaderboard.GetFormattedScoreListFromString("1 3 4 1 10 2 3");
        var expectedData = new List<int> {1, 3, 4, 1, 10, 2, 3};
        Assert.AreEqual(expectedData, data);
    }
    
    [Test]
    public void LeaderBoardControllerGettingFormattedListTest2()
    {
        var data = _leaderboard.GetFormattedScoreListFromString("");
        var expectedData = new List<int>{};
        Assert.AreEqual(expectedData, data);
    }
    
    [Test]
    public void LeaderBoardControllerGettingOrderedListTest()
    {
        var data = _leaderboard.GetOrderedScoreListFromString("1 3 4 1 10 2 3");
        var expectedData = new List<int> {10, 4, 3, 3, 2, 1, 1};
        Assert.AreEqual(expectedData, data);
    }
    
    [Test]
    public void LeaderBoardControllerGettingOrderedListTest2()
    {
        var data = _leaderboard.GetOrderedScoreListFromString("");
        var expectedData = new List<int> {};
        Assert.AreEqual(expectedData, data);
    }
    
}
