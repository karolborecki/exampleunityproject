using PoolNamespace;
using UnityEngine;

public class Bullet : PooledObj
{
    private float _speed;
    public float Damage { get; private set; }
    private Vector3 _dir;
    private bool _isInMovement;

    public override void OnObjSpawn()
    {
        StopShoot();
    }
    
    public override void OnObjDisappear()
    {
        StopShoot();
    }

    private void Update()
    {
        if (!_isInMovement) return;
        transform.position += (Vector3)(_dir * _speed * Time.deltaTime);
    }

    public void StartShoot(float speed, float dmg, Vector3 direction)
    {
        _speed = speed;
        Damage = dmg;
        _dir = direction;
        _isInMovement = true;
    }

    public void StopShoot()
    {
        _speed = 0;
        Damage = 0;
        _isInMovement = false;
    }

}
