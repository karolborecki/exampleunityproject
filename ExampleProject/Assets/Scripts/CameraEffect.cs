using UnityEngine;

public class CameraEffect : MonoBehaviour
{
    public Animator cameraAnimator;
    public Animator handlerAnimator;

    #region Singleton
    public static CameraEffect Instance;

    private void Awake()
    {
        Instance = this;
    }
    #endregion
    
    public void ApplyHitEffect()
    {
        cameraAnimator.SetTrigger("Hit");
        Shake();
    }

    public void Shake()
    {
        handlerAnimator.SetTrigger("Shake");
    }
}
