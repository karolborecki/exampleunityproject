using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public SpawnerConfig config;
    public bool isSpawning { get; private set; }

    private IEnumerator _spawnCoroutine;

    private void Start()
    {
        _spawnCoroutine = SpawnSequence();
    }

    public void StartSpawning()
    {
        StartCoroutine(_spawnCoroutine);
        isSpawning = true;
    }

    public void StopSpawning()
    {
        StopCoroutine(_spawnCoroutine);
        isSpawning = false;
    }

    private IEnumerator SpawnSequence()
    {
        for (;;)
        {
            Spawn();
            yield return new WaitForSeconds(config.SpawnDelay);
        }
    }

    private void Spawn()
    {
        ObjectPooler.Instance.SpawnFromPool("enemy", GetRandomSpawnPos(), Quaternion.identity);
    }

    private Vector2 GetRandomSpawnPos()
    {
        return new Vector2(Random.Range(-config.SpawnPosition.x, config.SpawnPosition.x), config.SpawnPosition.y);
    }
}
