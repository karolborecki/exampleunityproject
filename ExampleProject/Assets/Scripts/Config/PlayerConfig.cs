using System;
using UnityEngine;

public enum ShootingPattern {Single, Cone}

[Serializable]
public class PlayerConfig
{
    public PlayerController playerPrefab;
    public Vector2 playerStartPos;

    public ShootingPattern shootingPattern;
    public int conePatternBulletsCount = 5;

    public int maxBulletsInMag = 24;
    public float reloadTime = 1f;
    
    public int Hitpoints;
    public float Speed;
    public float BulletDamage;
    public float BulletSpeed;
    public float BulletCooldown;

    
}
