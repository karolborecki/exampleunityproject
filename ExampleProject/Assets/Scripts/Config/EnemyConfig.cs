using System;
using UnityEngine;

[Serializable]
public class EnemyConfig
{
    public int Hitpoints;
    public float Speed;
    public float Damage;
    
    public float uniqueUpdateTime = .1f;
    [Min(0)]
    public float uniqueDirectionX = .7f; 
}
