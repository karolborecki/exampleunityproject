using System;
using System.Collections;
using JetBrains.Annotations;
using PoolNamespace;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    [NotNull]
    public PooledObj bulletPrefab;
    public PlayerConfig config;

    private float _hitPointsLeft;
    private int _bulletsLeft = 24;
    private bool _isReloading;
    
    private IEnumerator _shootCoroutine;
    private Animator _animator;
    
    private void Start()
    {
        ApplyInputObservers();
        SetupAnimator();

        SetupShootingMagazines();
    }
    
    private void HandleShootInput(bool isShooting)
    {
        if(_shootCoroutine == null) return;
        if (isShooting) StartCoroutine(_shootCoroutine);
        else StopCoroutine(_shootCoroutine);
        
    }

    private void Move(Vector2 moveVec)
    {
        transform.Translate(moveVec * config.Speed * Time.deltaTime);
    }
    
    IEnumerator Shoot()
    {
        for (;;)
        {
            if(_bulletsLeft < 1) Reload();
            else
            {
                switch (config.shootingPattern)
                {
                    case ShootingPattern.Single:
                        SingleShoot();
                        break;
                    case ShootingPattern.Cone:
                        ConeShoot(_bulletsLeft > config.conePatternBulletsCount ? config.conePatternBulletsCount : _bulletsLeft);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                _animator.SetTrigger("Shoot");
            }

            yield return new WaitForSeconds(config.BulletCooldown);
        }
    }

    private void SingleShoot()
    {
        var bullet = ObjectPooler.Instance.SpawnFromPool("bullet", transform.position, Quaternion.identity).GetComponent<Bullet>();
        bullet.StartShoot(config.BulletSpeed, config.BulletDamage, Vector3.up);
        
        BulletShotFromMag(1);
    }

    private void ConeShoot(int bulletsCount)
    {
        var startDir = bulletsCount * .25f / 2;
        for (var i = 0; i < bulletsCount; i++)
        {
            var bullet = ObjectPooler.Instance.SpawnFromPool("bullet", transform.position, Quaternion.identity).GetComponent<Bullet>();
            bullet.StartShoot(config.BulletSpeed, config.BulletDamage, new Vector3(-startDir + 0.25f*i, 1));
        }
        
        BulletShotFromMag(bulletsCount);
    }

    private void ChangeShootPattern()
    {
        config.shootingPattern = config.shootingPattern == ShootingPattern.Single ? ShootingPattern.Cone : ShootingPattern.Single;
        UIController.Instance.SetShootingPatternText(config.shootingPattern);
    }

    private void BulletShotFromMag(int count)
    {
        _bulletsLeft -= count;
        UIController.Instance.SetBulletsLeftText(_bulletsLeft);
    }

    private void Reload()
    {
        if(!_isReloading)
        {
            UIController.Instance.OnReload();
            Invoke(nameof(SetBulletsLeftToMax), config.reloadTime);
        }
        _isReloading = true;
    }

    private void SetupShootingMagazines()
    {
        UIController.Instance.SetMaxBulletsText(config.maxBulletsInMag);
        SetBulletsLeftToMax();
    }

    private void SetBulletsLeftToMax()
    {
        _bulletsLeft = config.maxBulletsInMag;
        UIController.Instance.SetBulletsLeftText(_bulletsLeft);
        UIController.Instance.OnReloadEnd();
        _isReloading = false;
    }

    public void OnGameStart()
    {
        gameObject.SetActive(true);
        
        _shootCoroutine = Shoot();
        _hitPointsLeft = config.Hitpoints;
    }

    public void OnGameEnd()
    {
        gameObject.SetActive(false);

        _shootCoroutine = null;
        _hitPointsLeft = 0;
    }

    private void OnHit(float hitPoints)
    {
        _hitPointsLeft -= hitPoints;
        UIController.Instance.SetHealthBar(_hitPointsLeft, config.Hitpoints);
        CameraEffect.Instance.ApplyHitEffect();
        if (_hitPointsLeft <= 0) Die();
    }

    private void Die()
    {
        GameManager.Instance.GameEnd();
    }
    
    private void ApplyInputObservers()
    {
        InputManager.Instance.AddMoveInputObserver(Move);
        InputManager.Instance.AddShootInputObserver(HandleShootInput);
        InputManager.Instance.AddShootPatternInputObserver(ChangeShootPattern);
    }

    private void SetupAnimator()
    {
        _animator = GetComponent<Animator>();
        _animator.speed = 1 / config.BulletCooldown;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (!col.gameObject.CompareTag("Enemy")) return;
        var enemy = col.gameObject.GetComponent<EnemyController>();
        OnHit(enemy.config.Damage);
        ObjectPooler.Instance.BackToPool(enemy);
    }
}
