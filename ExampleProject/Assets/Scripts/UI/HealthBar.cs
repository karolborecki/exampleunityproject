using UnityEngine;

public class HealthBar : MonoBehaviour
{
    public float smoothStep = .1f;
    private float _maxSize;
    private Vector2 _desiredSize;

    private void Start()
    {
        _maxSize = transform.localScale.x;
        
        Reset();
    }

    private void Update()
    {
        SetSizeDelta(Vector2.Lerp(transform.localScale, _desiredSize, smoothStep*Time.deltaTime));
    }

    public void SetHealthPoints(float points, float maxPoints)
    {
        var desiredSizeX = points / maxPoints * _maxSize;
        SetDesiredSize(desiredSizeX);
    }

    private void SetSizeDelta(Vector2 size)
    {
        if (size.x > _maxSize || size.x < 0) return;
        transform.localScale = size;
    }

    private void SetDesiredSize(float desiredSizeX)
    {
        _desiredSize = new Vector2(desiredSizeX, transform.localScale.y);
    }

    public void Reset()
    {
        SetHealthPoints(1,1);
    }
}
