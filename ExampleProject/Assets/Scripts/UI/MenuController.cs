using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public Button playBtn;
    public Button leaderboardBtn;
    public Button instructionBtn;
    public Button exitBtn;

    public Button leaderboardBackBtn;
    public Button desktopInstructionBackBtn;
    public Button mobileInstructionBackBtn;

    public GameObject mainPanel;
    public GameObject leaderboardPanel;
    public GameObject mobileInstruction;
    public GameObject desktopInstruction;

    private GameObject _actualPlatformInstruction;

    private void Start()
    {
        CheckPlatform();
        AssignBtnListeners();
        
        mobileInstruction.SetActive(false);
        desktopInstruction.SetActive(false);
        
        MainPanel();
    }
    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    private void MainPanel()
    {
        SetMainPanelActive(true);
        SetLeaderboardActive(false);
        SetInstructionActive(false);
    }

    private void Leaderboard()
    {
        SetMainPanelActive(false);
        SetLeaderboardActive(true);
        SetInstructionActive(false);
    }

    private void Instruction()
    {
        SetMainPanelActive(false);
        SetLeaderboardActive(false);
        SetInstructionActive(true);
    }
    
    public void Exit()
    {
        Application.Quit();
    }

    private void CheckPlatform()
    {
        _actualPlatformInstruction = SystemInfo.deviceType == DeviceType.Handheld ? mobileInstruction : desktopInstruction;
    }
   
    private void SetMainPanelActive(bool active)
    {
        mainPanel.SetActive(active);
    }
    
    private void SetLeaderboardActive(bool active)
    {
        leaderboardPanel.SetActive(active);
    }

    private void SetInstructionActive(bool active)
    {
        _actualPlatformInstruction.SetActive(active);
    }

    private void AssignBtnListeners()
    {
        playBtn.onClick.AddListener(Play);
        leaderboardBtn.onClick.AddListener(Leaderboard);
        instructionBtn.onClick.AddListener(Instruction);
        exitBtn.onClick.AddListener(Exit);
        
        leaderboardBackBtn.onClick.AddListener(MainPanel);
        desktopInstructionBackBtn.onClick.AddListener(MainPanel);
        mobileInstructionBackBtn.onClick.AddListener(MainPanel);
    }
}
