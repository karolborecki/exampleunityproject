using System;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text scoreText;
    public HealthBar healthBar;
    public Text endPopupScoreText;
    public Text shootingPatternText;
    public Text maxBulletsInMagText;
    public Text bulletsLeftText;

    public GameObject startPanel;
    public GameObject gameOverPanel;
    public GameObject topBar;
    public GameObject shootInfoPanel;
    public GameObject reloadInfo;
    
    #region ListenersMethods
    private Action _onGameStartCallback;
    private Action _onGameCancelCallback;
    private Action _onBackToMainMenuCallback;
    #endregion
    
    public Button gameStartBtn;
    public Button gameCancelBtn;
    public Button backToMainMenuBtn;

    #region Singleton
    public static UIController Instance;

    private void Awake()
    {
        Instance = this;
    }
    #endregion

    private void Start()
    {
        SetTopBarActive(false);
        SetShootInfoPanelActive(false);
        SetReloadInfoActive(false);
        SetStartPanelActive(true);
        SetGameOverPanelActive(false);
        
        gameStartBtn.onClick.AddListener(OnStartGame);
        gameCancelBtn.onClick.AddListener(OnCancelGame);
        backToMainMenuBtn.onClick.AddListener(OnBackToMainMenu);
    }

    public void SetScoreText(int score)
    {
        scoreText.text = score.ToString();
    }
    
    public void SetHealthBar(float health, float maxHitPoints)
    {
        healthBar.SetHealthPoints(health, maxHitPoints);
    }

    public void SetEndPopupScore(int score)
    {
        endPopupScoreText.text = score.ToString();
    }
    
    public void SetShootingPatternText(ShootingPattern pattern)
    {
        shootingPatternText.text = pattern == ShootingPattern.Cone ? "Cone" : "Single";
    }
    
    public void SetBulletsLeftText(int bullets)
    {
        bulletsLeftText.text = bullets.ToString();
    }
    
    public void SetMaxBulletsText(int bullets)
    {
        maxBulletsInMagText.text = "\\" + bullets;
    }

    private void OnStartGame()
    {
        SetTopBarActive(true);
        SetShootInfoPanelActive(true);
        SetReloadInfoActive(false);
        SetStartPanelActive(false);
        SetGameOverPanelActive(false);
        _onGameStartCallback?.Invoke();
    }
    
    private void OnCancelGame()
    {
        _onGameCancelCallback?.Invoke();
    }
    
    private void OnBackToMainMenu()
    {
        _onBackToMainMenuCallback?.Invoke();
    }

    public void OnGameOver()
    {
        SetTopBarActive(false);
        SetReloadInfoActive(false);
        SetShootInfoPanelActive(false);
        SetStartPanelActive(false);
        SetGameOverPanelActive(true);
    }

    public void OnReload()
    {
        SetReloadInfoActive(true);
    }

    public void OnReloadEnd()
    {
        SetReloadInfoActive(false);
    }

    private void SetTopBarActive(bool active)
    {
        topBar.SetActive(active);
    }
    
    private void SetStartPanelActive(bool active)
    {
        startPanel.SetActive(active);
    }
    
    private void SetGameOverPanelActive(bool active)
    {
        gameOverPanel.SetActive(active);
    }
    
    private void SetShootInfoPanelActive(bool active)
    {
        shootInfoPanel.SetActive(active);
    }

    private void SetReloadInfoActive(bool active)
    {
        reloadInfo.SetActive(active);
    }

    #region ListenersMethods
    public void AddListenerOnGameStartEvent(Action listener)
    {
        _onGameStartCallback += listener;
    }
    
    public void RemoveListenerOnGameStartEvent(Action listener)
    {
        _onGameStartCallback -= listener;
    }
    
    public void AddListenerOnGameCancelEvent(Action listener)
    {
        _onGameCancelCallback += listener;
    }
    
    public void RemoveListenerOnGameCancelEvent(Action listener)
    {
        _onGameCancelCallback -= listener;
    }
    
    public void AddListenerOnBackToMainMenuEvent(Action listener)
    {
        _onBackToMainMenuCallback += listener;
    }
    
    public void RemoveListenerOnBackToMainMenuEvent(Action listener)
    {
        _onBackToMainMenuCallback -= listener;
    }
    #endregion
}
