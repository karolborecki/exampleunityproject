using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LeaderboardController : MonoBehaviour
{
    public LeaderboardElement elementPrefab;
    public GameObject scrollRect;

    private static int _leaderboardID = 1;
    private void Start()
    {
        var scoresList = GetOrderedScoreListFromString(GetScoreRawData());

        foreach(var score in scoresList)
        {
            var element = Instantiate(elementPrefab, scrollRect.transform);
            element.SetupElement(_leaderboardID, score);
            _leaderboardID++;
        }
    }
    
    public List<int> GetOrderedScoreListFromString(string scoreString)
    {
        var formattedList = GetFormattedScoreListFromString(scoreString);
        formattedList.Sort(CompareScore);

        return formattedList;
    }

    public List<int> GetFormattedScoreListFromString(string scoreString)
    {
        var formattedList = new List<int>(); 
        var subs = scoreString.Split(' ');

        if (subs.Length <= 0) return formattedList;
        formattedList.AddRange(from sub in subs where sub != "" select int.Parse(sub));

        return formattedList;
    }

    private static string GetScoreRawData()
    {
        return DataManager.GetScoreData();
    }

    private static int CompareScore(int score1, int score2)
    {
        return score2 - score1;
    }
}
