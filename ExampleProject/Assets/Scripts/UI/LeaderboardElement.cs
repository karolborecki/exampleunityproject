using UnityEngine;
using UnityEngine.UI;

public class LeaderboardElement : MonoBehaviour
{
    public Text idText;
    public Text pointsText;
    
    private int _id;
    private int _points;

    public void SetupElement(int id, int pts)
    {
        _id = id;
        _points = pts;
        UpdateText();
    }

    private void UpdateText()
    {
        idText.text = _id.ToString() + ".";
        pointsText.text = _points.ToString();
    }
}
