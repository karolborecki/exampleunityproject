using System.Collections;
using PoolNamespace;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class EnemyController : PooledObj
{
    public int scoreForKill = 1;
    public EnemyConfig config;

    public HealthBar healthBar;
    private float _hitPointsLeft;
    
    private bool _isMoving;
    private Vector3 _moveDirection;

    private Animator _animator;

    private IEnumerator _uniqueCoroutine;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _uniqueCoroutine = Unique();
    }

    public override void OnObjSpawn()
    {
        _hitPointsLeft = config.Hitpoints;
        
        SetupAnimSpeed();
        
        StartMoving();
        healthBar.Reset();

        StartCoroutine(_uniqueCoroutine);
    }

    public override void OnObjDisappear()
    {
        StopMoving();
        
        StopCoroutine(_uniqueCoroutine);
    }

    private void Update()
    {
        if (!_isMoving) return;
        
        transform.Translate(_moveDirection * config.Speed * Time.deltaTime);
    }
    
    private IEnumerator Unique()
    {
        for (;;)
        {
            PerformUnique();
            yield return new WaitForSeconds(config.uniqueUpdateTime);
        }
    }

    private void PerformUnique()
    {
        var hit = Physics2D.Raycast(transform.position, Vector3.down, 500, LayerMask.GetMask("Player"));
        _moveDirection = hit ? GetUniqueDirection() : GetDownDirection();
    }

    private Vector3 GetDownDirection()
    {
        return Vector3.down;
    }
    
    private Vector3 GetUniqueDirection()
    {
        return new Vector3(_moveDirection.x > 0 ? -config.uniqueDirectionX : config.uniqueDirectionX, -1);
    }

    private void OnHit(float dmg)
    {
        _hitPointsLeft -= dmg;
        healthBar.SetHealthPoints(_hitPointsLeft, config.Hitpoints);
        if (_hitPointsLeft <= 0) Die();
    }

    private void Die()
    {
        _animator.speed = 1;
        healthBar.SetHealthPoints(0, 1);
        _animator.SetTrigger("Die");
    }
    
    public void OnDieAnimEnd()
    {
        ObjectPooler.Instance.BackToPool(this);
        GameManager.Instance.AddScore(scoreForKill);
        SetupAnimSpeed();
    }

    private void SetupAnimSpeed()
    {
        _animator.speed = config.Speed;
    }

    private void StartMoving()
    {
        _isMoving = true;
    }
    
    private void StopMoving()
    {
        _isMoving = false;
    }
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.CompareTag("Bullet")) return;
        var bullet = col.gameObject.GetComponent<Bullet>();
        OnHit(bullet.Damage);
        ObjectPooler.Instance.BackToPool(bullet);
    }
}
