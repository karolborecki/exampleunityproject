using System;
using System.Data;
using PoolNamespace;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public PlayerConfig playerConfig;
    public SpawnerConfig spawnerConfig;
    public EnemyConfig enemyConfig;

    private PlayerController _player;
    public Spawner spawner;

    private int _actualScore = 0; 
    
    #region Singleton
    public static GameManager Instance;
    #endregion
    
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        AssignGameConfiguration();
        AssignPoolRequiredObjects();
        
        UIController.Instance.AddListenerOnGameStartEvent(GameStart);
        UIController.Instance.AddListenerOnGameCancelEvent(GoToMainMenu);
        UIController.Instance.AddListenerOnBackToMainMenuEvent(GoToMainMenu);
    }

    public void GameStart()
    {
        if(!spawner.isSpawning)
            spawner.StartSpawning();
        _player.OnGameStart();

        SetScore(0);
    }
    
    public void GameEnd()
    {
        if(spawner.isSpawning)
            spawner.StopSpawning();
        _player.OnGameEnd();
        UIController.Instance.OnGameOver();
        UIController.Instance.SetEndPopupScore(_actualScore);
        DataManager.AddScoreData(_actualScore);
    }

    public void GoToMainMenu()
    {
        ClearGameConfiguration();
        SceneManager.LoadScene(0);
    }

    public void AddScore(int score)
    {
        SetScore(_actualScore + score);
    }

    private void SetScore(int score)
    {
        _actualScore = score;
        UIController.Instance.SetScoreText(_actualScore);
    }

    private void AssignGameConfiguration()
    {
        InputManager.Instance ??= new GameObject("Input Manager").AddComponent<InputManager>();
        
        ObjectPooler.Instance ??= new GameObject("Object Pool").AddComponent<ObjectPooler>();

        if (UIController.Instance == null)
            throw new NoNullAllowedException("UIController not assigned!");
        
        spawner ??= new GameObject("Spawner").AddComponent<Spawner>();
        spawner.config = spawnerConfig;
        
        _player ??= Instantiate(playerConfig.playerPrefab, playerConfig.playerStartPos, Quaternion.identity);
        _player.config = playerConfig;
        _player.gameObject.SetActive(false);
    }

    private void AssignPoolRequiredObjects()
    {
        ObjectPooler.Instance.CreatePool("bullet", _player.bulletPrefab, 80); //TODO change size to smth non-static
        
        var enemyPrefab = spawnerConfig.Prefab;
        if (enemyPrefab == null) throw new ArgumentNullException("Wrong EnemyPrefab!");
        enemyPrefab.GetComponent<EnemyController>().config = enemyConfig;
        ObjectPooler.Instance.CreatePool("enemy", enemyPrefab.GetComponent<PooledObj>(), 30); //TODO change size to smth non-static

    }

    private void ClearGameConfiguration()
    {
        SetScore(0);
        
        InputManager.Instance = null;
        ObjectPooler.Instance = null;
        UIController.Instance = null;
        Instance = null;
    }
}
