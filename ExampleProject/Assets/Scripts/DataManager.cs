using UnityEngine;

public class DataManager : MonoBehaviour
{
    private const string ScoreDataTag = "scores";

    public static string GetScoreData()
    {
        var data = PlayerPrefs.GetString(ScoreDataTag);
        if (data == "No Name") data = "";
        return data;
    }

    public static void AddScoreData(int data)
    {
        if (data < 0) return;
        var scoreData = GetScoreData();
        SetScoresData(scoreData + data + " ");
    }
    
    public static void SetScoresData(string data)
    {
        PlayerPrefs.SetString(ScoreDataTag, data);
    }
}
