using System;
using System.Data;
using InputNamespace;
using PlatformInput;
using UnityEngine;

public class InputManager : MonoBehaviour, IInputManager
{
    private PlatformInput.PlatformInput _platformInput;
    
    private Action<Vector2> _onMoveInputAction;
    private Action<bool> _onShootInputAction;
    private Action _onShootPatternInputAction;

    private Vector2 _moveDir = new Vector2(0,0);

    #region Singleton
    public static IInputManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    private void Start()
    {
        CheckPlatformInput();
        
        if (_platformInput == null) 
            throw new NoNullAllowedException("No platform specified!");
    }

    private void Update()
    {
        HandleMoveInput();
        HandleShootInput();
        HandleShootPatternInput();
    }

    private void HandleMoveInput()
    {
        _moveDir = new Vector2(0, 0);
        if (_platformInput.MoveRightInput()) OnMoveInputRight();
        if (_platformInput.MoveLeftInput()) OnMoveInputLeft();
        _onMoveInputAction?.Invoke(_moveDir);
    }

    private void HandleShootInput()
    {
        if (_platformInput.ShootInputDown())
            OnShootInput(true);
        else if (_platformInput.ShootInputUp())
            OnShootInput(false);
    }
    
    private void HandleShootPatternInput()
    {
        if (_platformInput.ShootPatternInput())
            OnShotPatternInput();
    }

    private void OnMoveInputRight()
    {
        if(_moveDir.x < 1)
         _moveDir.x += 1;
    }

    private void OnMoveInputLeft()
    {
        if(_moveDir.x > -1)
            _moveDir.x -= 1;
    }

    private void OnShootInput(bool isShooting)
    {
        _onShootInputAction?.Invoke(isShooting);
    }

    private void OnShotPatternInput()
    {
        _onShootPatternInputAction?.Invoke();
    }

    private void ChangeInputPlatform(PlatformInput.PlatformInput input)
    {
        _platformInput = input;
    }

    private void CheckPlatformInput()
    {
        switch (SystemInfo.deviceType)
        {
            case DeviceType.Desktop:
                ChangeInputPlatform(new DesktopInput());
                break;
            case DeviceType.Handheld:
                ChangeInputPlatform(new MobileInput());
                break;
            case DeviceType.Unknown:
                ChangeInputPlatform(new MixedInput());
                break;
            case DeviceType.Console:
                throw new PlatformNotSupportedException("Game is not built for Consoles!");
            default:
                ChangeInputPlatform(new MixedInput());
                break;
        }
    }

    #region ObserversMethods
    public void AddMoveInputObserver(Action<Vector2> obs)
    {
        _onMoveInputAction += obs;
    }

    public void RemoveMoveInputObserver(Action<Vector2> obs)
    {
        _onMoveInputAction -= obs;
    }

    public void AddShootInputObserver(Action<bool> obs)
    {
        _onShootInputAction += obs;
    }

    public void RemoveShootInputObserver(Action<bool> obs)
    {
        _onShootInputAction -= obs;
    }

    public void AddShootPatternInputObserver(Action obs)
    {
        _onShootPatternInputAction += obs;
    }

    public void RemoveShootPatternIInputObserver(Action obs)
    {
        _onShootPatternInputAction -= obs;
    }

    #endregion
}
