using UnityEngine;

namespace PlatformInput
{
    public interface PlatformInput
    {
        bool MoveLeftInput();
        bool MoveRightInput();
        bool ShootInputDown();
        bool ShootInputUp();

        bool ShootPatternInput();
    }

    public class MobileInput : PlatformInput
    {
        private bool _isMoveLeftDown;
        private bool _isMoveRightDown;
        
        public bool MoveLeftInput()
        {
            if (CheckForTouchBetween(0, Camera.main.pixelWidth / 3, TouchPhase.Began))
                _isMoveLeftDown = true;
            else if (CheckForTouchBetween(0, Camera.main.pixelWidth / 3, TouchPhase.Ended))
                _isMoveLeftDown = false;

            return _isMoveLeftDown;
        }

        public bool MoveRightInput()
        {
            if (CheckForTouchBetween(2 * Camera.main.pixelWidth / 3, Camera.main.pixelWidth, TouchPhase.Began))
                _isMoveRightDown = true;
            else if (CheckForTouchBetween(2 * Camera.main.pixelWidth / 3, Camera.main.pixelWidth, TouchPhase.Ended))
                _isMoveRightDown = false;

            return _isMoveRightDown;
        }

        public bool ShootInputDown()
        {
            return CheckForTouchBetween(Camera.main.pixelWidth / 3, 2 * Camera.main.pixelWidth / 3, TouchPhase.Began);

        }

        public bool ShootInputUp()
        {
            return CheckForTouchBetween(Camera.main.pixelWidth / 3, 2 * Camera.main.pixelWidth / 3, TouchPhase.Ended);

        }

        public bool ShootPatternInput()
        {
            return false;
        }

        private static bool CheckForTouchBetween(int minX, int maxX, TouchPhase touchPhase)
        {
            if (Input.touchCount <= 0) return false;
            var touch = Input.GetTouch(0);
            return touch.phase == touchPhase && touch.position.x < maxX && touch.position.x > minX;
        }

    }
    
    public class DesktopInput : PlatformInput
    {
        public bool MoveLeftInput()
        {
            return Input.GetKey(KeyCode.A);
        }

        public bool MoveRightInput()
        {
            return Input.GetKey(KeyCode.D);
        }

        public bool ShootInputDown()
        {
            return Input.GetKeyDown(KeyCode.Space);
        }

        public bool ShootInputUp()
        {
            return Input.GetKeyUp(KeyCode.Space);
        }

        public bool ShootPatternInput()
        {
            return Input.GetKeyDown(KeyCode.S);
        }
    }

    public class MixedInput : PlatformInput
    {
        private readonly PlatformInput _mobileInput = new MobileInput();
        private readonly PlatformInput _desktopInput = new DesktopInput();
        public bool MoveLeftInput()
        {
            return _mobileInput.MoveLeftInput() || _desktopInput.MoveLeftInput();
        }

        public bool MoveRightInput()
        {
            return _mobileInput.MoveRightInput() || _desktopInput.MoveRightInput();
        }

        public bool ShootInputDown()
        {
            return _mobileInput.ShootInputDown() || _desktopInput.ShootInputDown();
        }

        public bool ShootInputUp()
        {
            return _mobileInput.ShootInputUp() || _desktopInput.ShootInputUp();
        }

        public bool ShootPatternInput()
        {
            return _mobileInput.ShootPatternInput() || _desktopInput.ShootPatternInput();
        }
    }
}