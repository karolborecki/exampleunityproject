using System;
using UnityEngine;

namespace InputNamespace
{
    public interface IInputManager
    {
        public void AddMoveInputObserver(Action<Vector2> obs);
        public void RemoveMoveInputObserver(Action<Vector2> obs);

        public void AddShootInputObserver(Action<bool> obs);
        public void RemoveShootInputObserver(Action<bool> obs);
        
        public void AddShootPatternInputObserver(Action obs);
        public void RemoveShootPatternIInputObserver(Action obs);

    }
}