using System;
using System.Collections.Generic;
using PoolNamespace;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public PooledObj prefab;
        public int size;

        public Pool(string tag, PooledObj prefab, int size)
        {
            this.tag = tag;
            this.prefab = prefab;
            this.size = size;
        }
    }
    #region Singleton
    public static ObjectPooler Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion
    
    private void Start()
    {
        InitPoolingDic();
    }

    public List<Pool> pools = new List<Pool>();
    private readonly Dictionary<string, Queue<PooledObj>> _poolDictionary = new Dictionary<string, Queue<PooledObj>>();
    
    private void InitPoolingDic()
    {
        foreach (var pool in pools)
            InitPool(pool);
    }

    public PooledObj SpawnFromPool(string tag, Vector3 pos, Quaternion rot)
    {
        if (!_poolDictionary.ContainsKey(tag)) return null;
        var objToSpawn = _poolDictionary[tag].Dequeue();
        objToSpawn.transform.position = pos;
        objToSpawn.transform.rotation = rot;
        objToSpawn.gameObject.SetActive(true);
        
        var pooledObj = objToSpawn.GetComponent<PooledObj>();
        pooledObj.OnObjSpawn();

        _poolDictionary[tag].Enqueue(objToSpawn);
        return objToSpawn;
    }

    public void BackToPool(PooledObj obj)
    {
        if (obj == null) return;
        obj.gameObject.SetActive(false);
        obj.OnObjDisappear();
    }

    public void CreatePool(string tag, PooledObj prefab, int size)
    {
        InitPool(new Pool(tag, prefab, size));
    }

    public void InitPool(Pool pool)
    {
        if (ContainsTag(pool.tag)) return;
        pools.Add(pool);
        var objPool = new Queue<PooledObj>();
        
        for (var i = 0; i < pool.size; i++)
        {
            var obj = Instantiate(pool.prefab).GetComponent<PooledObj>();
            obj.gameObject.SetActive(false);
            objPool.Enqueue(obj);
        }
        _poolDictionary.Add(pool.tag, objPool);
    }

    public bool ContainsTag(string tag)
    {
        return _poolDictionary.ContainsKey(tag);
    }
}
